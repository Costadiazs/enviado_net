namespace Enviado.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Enviado.Models;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<Enviado.Models.EnviadoNetContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Enviado.Models.EnviadoNetContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            var remitos = new List<Remito>
            {
                new Remito { cliente = "Santiago", numero = "1200", fecha = DateTime.Parse("2016-03-23") },
                new Remito { cliente = "Maqui", numero = "1201", fecha = DateTime.Parse("2016-03-25") },
                new Remito { cliente = "Roberto", numero = "1202", fecha = DateTime.Parse("2016-03-24") }
            };
            remitos.ForEach(s => context.Remitos.Add(s));
            context.SaveChanges(); 
        }
    }
}
