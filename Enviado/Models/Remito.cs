﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Enviado.Models
{
    public class Remito
    {
        public int ID { get; set; }
        public string numero { get; set; }
        public DateTime fecha { get; set; }
        public string cliente { get; set; }
    }
}

