﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Enviado.Models
{
    public class EnviadoNetContext : DbContext
    {
        public EnviadoNetContext()
            : base("EnviadoConnection")
        {
        }
       
        public DbSet<Remito> Remitos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }

    }
}
