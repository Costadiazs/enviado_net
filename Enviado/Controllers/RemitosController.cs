﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Enviado.Models;

namespace Enviado.Controllers
{
    public class RemitosController : ApiController
    {
        public Remito Get(int id)
        {
            EnviadoNetContext db = new EnviadoNetContext();
            Remito remito = db.Remitos.Find(id);
            return remito;

        }
    }
}
